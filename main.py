import MatrixBuilder as matrix_Builder
import CompleteMatrixBuilder as complete_matrix_builder
import FolderType as ft
import ConvolutionalNeuralNetwork as cnn
import Window as window
import tensorflow as tf
import TrainPreTrainedModel as tptm
import warnings

warnings.simplefilter('ignore')

trainTheModel = False
trainExistingModel = False


numOfFirstLayerFilters = 32

modelPath = "saved_model/my_model" + str(numOfFirstLayerFilters)

if trainTheModel:

    train_SFWMatrixBuilder = matrix_Builder.MatrixBuilder(folderPath="C:/Users/allan/Desktop/nsfw dataset/train/SFW", folderType=ft.FolderType.SFW, numberOfSamples=16000) #Max imaages = 121611
    train_NSFWMatrixBuilder = matrix_Builder.MatrixBuilder(folderPath="C:/Users/allan/Desktop/nsfw dataset/train/NSFW", folderType=ft.FolderType.NSFW, numberOfSamples=16000) #max images = 112373

    val_SFWMatrixBuilder = matrix_Builder.MatrixBuilder(folderPath="C:/Users/allan/Desktop/nsfw dataset/val/SFW", folderType=ft.FolderType.SFW, numberOfSamples=500)
    val_NSFWMatrixBuilder = matrix_Builder.MatrixBuilder(folderPath="C:/Users/allan/Desktop/nsfw dataset/val/NSFW", folderType=ft.FolderType.NSFW, numberOfSamples=500)

    train_SFWListOfCoordinates = train_SFWMatrixBuilder.getAppendedListOfCoordinates()
    train_SFWListOfLabels = train_SFWMatrixBuilder.getAppendedListOfLabels()
    train_NSFWListOfCoordinates = train_NSFWMatrixBuilder.getAppendedListOfCoordinates()
    train_NSFWListOfLabels = train_NSFWMatrixBuilder.getAppendedListOfLabels()

    val_SFWListOfCoordinates = val_SFWMatrixBuilder.getAppendedListOfCoordinates()
    val_SFWListOfLabels = val_SFWMatrixBuilder.getAppendedListOfLabels()
    val_NSFWListOfCoordinates = val_NSFWMatrixBuilder.getAppendedListOfCoordinates()
    val_NSFWListOfLabels = val_NSFWMatrixBuilder.getAppendedListOfLabels()


    train_completeMatrixBuilder = complete_matrix_builder.CompleteMatrixBuilder(SFWCoordinateList=train_SFWListOfCoordinates,
                                                                                NSFWCoordinateList=train_NSFWListOfCoordinates,
                                                                                SFWLabelList=train_SFWListOfLabels,
                                                                                NSFWLabelList=train_NSFWListOfLabels
                                                                                )
    train_completeMatrixBuilder.buildInputMatrix()
    train_completeMatrixBuilder.buildLabelMatrix()

    val_completeMatrixBuilder = complete_matrix_builder.CompleteMatrixBuilder(SFWCoordinateList=val_SFWListOfCoordinates,
                                                                              NSFWCoordinateList=val_NSFWListOfCoordinates,
                                                                              SFWLabelList=val_SFWListOfLabels,
                                                                              NSFWLabelList=val_NSFWListOfLabels,
                                                                              )
    val_completeMatrixBuilder.buildInputMatrix()
    val_completeMatrixBuilder.buildLabelMatrix()

    train_compeletedInputMatrix = train_completeMatrixBuilder.getCompleteInputMatrix()
    train_completedLabelMatrix = train_completeMatrixBuilder.getCompleteLabelMatrix()

    val_completedInputMatrix = val_completeMatrixBuilder.getCompleteInputMatrix()
    val_completedLabelMatrix = val_completeMatrixBuilder.getCompleteLabelMatrix()

    if trainExistingModel:
        loadedModel = tf.keras.models.load_model(modelPath)
        selectedNetwork = tptm.TrainPreTrainedModel(x_train=train_compeletedInputMatrix,
                                                    x_val=val_completedInputMatrix,
                                                    y_train=train_completedLabelMatrix,
                                                    y_val=val_completedLabelMatrix,
                                                    train_batch_size=128,
                                                    val_batch_size=100,
                                                    model=loadedModel,
                                                    saveDestination=modelPath
                                                    )
    else:
        selectedNetwork = cnn.ConvolutionalNeuralNetwork(x_train=train_compeletedInputMatrix,
                                                         x_val=val_completedInputMatrix,
                                                         y_train=train_completedLabelMatrix,
                                                         y_val=val_completedLabelMatrix,
                                                         train_batch_size=128,
                                                         val_batch_size=100,
                                                         saveDestination=modelPath,
                                                         numOfFiltersInFirstLayer=numOfFirstLayerFilters)
        selectedNetwork.defineTheModel()
        pass
    selectedNetwork.displayModelSumary()
    selectedNetwork.compileTheModel()
    selectedNetwork.createDatasets()
    selectedNetwork.evaluateModel()
    selectedNetwork.trainTheNetwork(numOfIterations=5)
    theModel = selectedNetwork.getModel()
else:
    theModel = tf.keras.models.load_model(modelPath)
    pass



myWindow = window.Window(theModel)

#Val Accuracy peaked at 0.86 with 8 filters
#Val Accuracy peaked at 0.9085 with 16 filters
#Val Accuracy peaked at 0.90 with 32 filters

