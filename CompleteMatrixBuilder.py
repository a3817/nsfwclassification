import MatrixBuilder as matrix_builder
import numpy as np

class CompleteMatrixBuilder:
    def __init__(self, SFWCoordinateList, NSFWCoordinateList, SFWLabelList, NSFWLabelList): #Still need to append the list of labels here and then output a final list
        self.completeInputMatrix = None
        self.completeLabelMatrix = None

        self.SFWCoordinateList = SFWCoordinateList
        self.NSFWCoordinateList = NSFWCoordinateList

        self.SFWLabelList = SFWLabelList
        self.NSFWLabelList = NSFWLabelList


        self.completeInputlist = []
        self.completeLabelList = []
        pass


    def buildInputMatrix(self):

        for imageArray in self.SFWCoordinateList:
            self.completeInputlist.append(imageArray)
            pass
        for imageArray in self.NSFWCoordinateList:
            self.completeInputlist.append(imageArray)
            pass

        self.completeInputMatrix = np.stack(self.completeInputlist, axis=0)

        pass

    def buildLabelMatrix(self):
        for value in self.SFWLabelList:
            self.completeLabelList.append(value)
            pass

        for value in self.NSFWLabelList:
            self.completeLabelList.append(value)
            pass

        self.completeLabelMatrix = np.stack(self.completeLabelList, axis=0)
        pass

    def getCompleteInputMatrix(self):
        return self.completeInputMatrix

    def getCompleteLabelMatrix(self):
        return self.completeLabelMatrix
