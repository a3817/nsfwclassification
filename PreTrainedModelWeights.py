from tensorflow import keras
from keras import layers
import tensorflow as tf

class PreTrainedModelWeights:
    def __init__(self, image_size):
        self.image_size = image_size
        self.img_inputs = tf.keras.Input(shape=(self.image_size, self.image_size, 3))
        pass

    def defineTheModel(self):
        # Convolutional layers first
        conv2dLayer0 = layers.Conv2D(padding='same', kernel_size=(3, 3), strides=(1, 1), filters=5)(self.img_inputs)
        reluLayer0 = layers.ReLU()(conv2dLayer0)
        maxPoolLayer0 = layers.MaxPool2D(padding='valid', strides=(2, 2), pool_size=(2, 2))(reluLayer0)

        conv2dLayer1 = layers.Conv2D(padding='same', kernel_size=(3, 3), strides=(1, 1), filters=10)(maxPoolLayer0)
        reluLayer1 = layers.ReLU()(conv2dLayer1)
        maxPoolLayer1 = layers.MaxPool2D(padding='valid', strides=(2, 2), pool_size=(2, 2))(reluLayer1)

        conv2dLayer2 = layers.Conv2D(padding='same', kernel_size=(3, 3), strides=(1, 1), filters=20)(maxPoolLayer1)
        reluLayer2 = layers.ReLU()(conv2dLayer2)
        maxPoolLayer2 = layers.MaxPool2D(padding='valid', strides=(2, 2), pool_size=(2, 2))(reluLayer2)

        # Fully connected/flattened layers
        layer1 = layers.Flatten()(maxPoolLayer2)
        layer2Z = layers.Dense(50)(layer1)
        layer2A = tf.keras.layers.ReLU()(layer2Z)
        layer3 = layers.Dropout(0.2)(layer2A)
        layer4A = layers.Dense(30)(layer3)
        layer4Z = layers.ReLU()(layer4A)
        layer5 = layers.Dropout(0.2)(layer4Z)
        layer6 = layers.Dense(2)(layer5)
        outputLayer = layers.Softmax()(layer6)

        self.model = keras.Model(inputs=self.img_inputs, outputs=outputLayer, name='mnist_model')

        pass

    def getPreTrainedModel(self):
        return self.model