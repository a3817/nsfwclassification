import os


class FolderHandler:
    def __init__(self, path):
        self.path = path
        self.listOfPathNames = []
        self.fillListOfNames()
        pass

    def fillListOfNames(self):
        for file in os.listdir(self.path):
            self.listOfPathNames.append(self.path + "/" + file)
            pass
        pass

    def getListOfPaths(self):
        return self.listOfPathNames


