import os.path
import tensorflow as tf
import tkinter as tk
from tkinter import *
import ImageHandler as image_handler
from PIL import ImageTk, Image
import numpy as np
from tkinter import filedialog
import FolderType as ft

class GraphicaluserInterface:
    def __init__(self, root, model):
        self.model = model
        self.root = root
        self.currentlySelectedImagehandler = None
        self.root.title("NSFW classification")
        self.imageFrame = Frame(self.root, width= 600, height = 600)
        self.imageFrame.place(x=100, y = 100)

        self.imageLabel = Frame(self.imageFrame)
        self.image = None #ImageTk.PhotoImage(Image.open("C:/Users/allan/Desktop/nsfw dataset/train/SFW/train_neutral6261.jpg"))
        self.imageLabel = Label(self.imageFrame, image=self.image)
        self.addPredictButton()
        self.addSelectImageButton()
        self.addPredictionLabels()
        self.placePredictionLabels()
        self.setWindowDimensions()


    def setWindowDimensions(self):
        self.root.geometry("800x800")
        pass

    def addPredictButton(self):
        self.predictButton = tk.Button(self.root, text="Predict!", command=self.getPrediction)
        self.predictButton.place(x = 0, y = 0)

        pass

    def addSelectImageButton(self):
        self.selectImageButton = tk.Button(self.root, text="Select Image", command=self.selectAnImage)
        self.selectImageButton.place(x = 100, y = 0)
        pass

    def addPredictionLabels(self):
        self.class0PredictionLabel = Label(self.root, text = "SFW: ")
        self.class1PredictionLabel = Label(self.root, text="NSFW: ")
        self.conclusionLabel = Label(self.root, text="")

        pass

    def placePredictionLabels(self):
        self.class0PredictionLabel.place(x= 0, y = 30)
        self.class1PredictionLabel.place(x=0, y=60)
        self.conclusionLabel.place(x=580, y = 30)

        pass

    def updatePredictionLabels(self, predictionArray):
        self.class0PredictionLabel.config(text = "SFW: " + str(predictionArray[0][0]))
        self.class1PredictionLabel.config(text="NSFW: " + str(predictionArray[0][1]))

        if self.predictionConclusion == 0:
            self.conclusionLabel.config(text="SFW", fg="#00ff00", font=("Arial", 25))
        elif self.predictionConclusion == 1:
            self.conclusionLabel.config(text="NSFW", fg="#ff0000", font=("Arial", 25))

        pass


    def selectAnImage(self):
        file = filedialog.askopenfile()
        path = str(os.path.abspath(file.name))
        self.currentlySelectedImagehandler = image_handler.ImageHandler(path)
        self.placeImageInWindow(path)

        pass

    def placeImageInWindow(self, path): #Inactive at the moment
        img = Image.open(path)
        img = img.resize((600, 600))
        self.image = ImageTk.PhotoImage(img)

        self.imageLabel.config(image=self.image)


        self.imageLabel.pack()
        pass


    def getPrediction(self):
        #Get inputs for prediction here. have to be pixel values of the image
        #self.model.predict()
        ImageArray = np.array(self.currentlySelectedImagehandler.get_image_array())
        ImageArray.resize(224, 224, 3)
        ImageArray = ImageArray.reshape(1,224, 224, 3)
        #self.currentlySelectedImagehandler.plotImage()
        prediction = self.model.predict(ImageArray)
        self.predictionConclusion = np.argmax(prediction)
        self.updatePredictionLabels(predictionArray=prediction)

        pass