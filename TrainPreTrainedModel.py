#import os

#os.environ["CUDA_VISIBLE_DEVICES"] = "-1"




import tensorflow as tf
class TrainPreTrainedModel:
    def __init__(self, x_train, y_train, x_val, y_val, train_batch_size, val_batch_size, model, saveDestination):
        self.x_train = x_train
        self.y_train = y_train
        self.x_val = x_val
        self.y_val = y_val

        #self.validation_batch_size = validation_batch_size
        self.image_size = x_train.shape[1] #Height or width of the image
        self.numOfDimensions = x_train.shape[3]
        self.batch_Size = train_batch_size
        self.val_batch_size = val_batch_size
        self.y_train = tf.one_hot(self.y_train, 2)
        self.y_val = tf.one_hot(self.y_val, 2)

        self.img_inputs = tf.keras.Input(shape = (self.image_size, self.image_size, 3))


        self.model = model
        self.saveDestination = saveDestination
        pass

    def displayModelSumary(self):
        self.model.summary()
        pass

    def compileTheModel(self):
        lossFunction = tf.keras.losses.CategoricalCrossentropy(from_logits=False)
        optimizer = tf.keras.optimizers.Adam()
        self.model.compile(optimizer=optimizer, loss=lossFunction, metrics=['accuracy'])
        pass

    def createDatasets(self):
        self.train_dataset = tf.data.Dataset.from_tensor_slices((self.x_train, self.y_train)).batch(batch_size=self.batch_Size)
        self.val_dataset = tf.data.Dataset.from_tensor_slices((self.x_val, self.y_val)).batch(
            batch_size=self.val_batch_size)
        self.train_dataset = self.train_dataset.shuffle(32000)
        pass

    def trainTheNetwork(self, numOfIterations):
        self.model.fit(self.train_dataset, epochs=numOfIterations, validation_data=self.val_dataset)
        self.saveWholeModel()
        pass

    def saveWholeModel(self):
        self.model.save(self.saveDestination)

    def evaluateModel(self):
        self.model.evaluate(self.x_val, self.y_val, batch_size=self.val_batch_size)

    def getModel(self):
        return self.model