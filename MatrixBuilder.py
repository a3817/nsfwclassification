import FolderHandler as folder_handler
import ImageHandler as image_handler
import random
import numpy as np

class MatrixBuilder:
    def __init__(self, folderPath, folderType, numberOfSamples):
        self.folderPath = folderPath
        self.listOfImagePaths = []
        self.appendedListOfCoordinates = []
        self.appendedListOfLabels = []
        self.folderType = folderType
        print("Building for " + str(folderType))
        self.numberOfSamples = numberOfSamples
        self.setListOfImagePaths()
        pass

    def setListOfImagePaths(self):
        #Test with just 2 images
        self.folderHandler = folder_handler.FolderHandler(self.folderPath)
        self.listOfImagePaths = self.folderHandler.getListOfPaths()
        listOfIntegers = random.sample(range(len(self.listOfImagePaths)), self.numberOfSamples)
        tracker = 0
        for index in listOfIntegers:
            path = self.listOfImagePaths[index]
            currentImageHandler = image_handler.ImageHandler(path)
            currentArray = currentImageHandler.get_image_array()
            if(currentArray.shape == (224, 224, 3)):
                self.appendedListOfCoordinates.append(currentArray) #Input list
                self.appendedListOfLabels.append(self.folderType.value) #label list
                pass
            tracker += 1

            pass
        #self.stackedArray = np.stack(intermediateList, axis = 0)
        #print(self.stackedArray.shape)
        pass


    def getAppendedListOfCoordinates(self):
        return self.appendedListOfCoordinates

    def getAppendedListOfLabels(self):
        return self.appendedListOfLabels


