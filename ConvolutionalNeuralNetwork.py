import tensorflow as tf
from tensorflow import keras
from keras import layers
#import os

#os.environ["CUDA_VISIBLE_DEVICES"] = "-1"


class ConvolutionalNeuralNetwork:
    def __init__(self, x_train, y_train, x_val, y_val, train_batch_size, val_batch_size, saveDestination, numOfFiltersInFirstLayer):
        self.x_train = x_train
        self.y_train = y_train
        self.x_val = x_val
        self.y_val = y_val

        #self.validation_batch_size = validation_batch_size
        self.image_size = x_train.shape[1] #Height or width of the image
        self.numOfDimensions = x_train.shape[3]
        self.train_batch_Size = train_batch_size
        self.val_batch_size = val_batch_size
        self.y_train = tf.one_hot(self.y_train, 2)
        self.y_val = tf.one_hot(self.y_val, 2)
        self.img_inputs = tf.keras.Input(shape = (self.image_size, self.image_size, 3))

        #self.checkPointPath = "checkPointDirectory/training_1/cp.ckpt"
        #self.checkPointDirectoryPath = os.path.dirname(self.checkPointPath)
        #self.checkPointCallback = tf.keras.callbacks.ModelCheckpoint(filepath = self.checkPointPath, save_weights_only=True, verbose=1)

        self.saveDestination = saveDestination
        self.numOfiltersInFirstLayer = numOfFiltersInFirstLayer
        pass

    def defineTheModel(self):
        # Convolutional layers first

        conv2dLayer0 = layers.Conv2D(padding='same', kernel_size=(3, 3), strides=(1, 1), filters=self.numOfiltersInFirstLayer)(self.img_inputs)
        reluLayer0 = layers.ReLU()(conv2dLayer0)
        maxPoolLayer0 = layers.MaxPool2D(padding='valid', strides=(2, 2), pool_size=(2, 2))(reluLayer0)

        conv2dLayer1 = layers.Conv2D(padding='same', kernel_size=(3, 3), strides=(1, 1), filters=self.numOfiltersInFirstLayer*2)(maxPoolLayer0)
        reluLayer1 = layers.ReLU()(conv2dLayer1)
        maxPoolLayer1 = layers.MaxPool2D(padding='valid', strides=(2, 2), pool_size=(2, 2))(reluLayer1)

        conv2dLayer2 = layers.Conv2D(padding='same', kernel_size=(3, 3), strides=(1, 1), filters=self.numOfiltersInFirstLayer*4)(maxPoolLayer1)
        reluLayer2 = layers.ReLU()(conv2dLayer2)
        maxPoolLayer2 = layers.MaxPool2D(padding='valid', strides=(2, 2), pool_size=(2, 2))(reluLayer2)

        conv2dLayer3 = layers.Conv2D(padding='same', kernel_size=(3, 3), strides=(1, 1), filters=self.numOfiltersInFirstLayer*8)(maxPoolLayer2)
        reluLayer3 = layers.ReLU()(conv2dLayer3)
        maxPoolLayer3 = layers.MaxPool2D(padding='valid', strides=(2, 2), pool_size=(2, 2))(reluLayer3)



        # Fully connected/flattened layers
        layer1 = layers.Flatten()(maxPoolLayer3)
        layer2Z = layers.Dense(50)(layer1)
        layer2A = tf.keras.layers.ReLU()(layer2Z)
        layer3 = layers.Dropout(0.2)(layer2A)
        layer4A = layers.Dense(30)(layer3)
        layer4Z = layers.ReLU()(layer4A)
        layer5 = layers.Dropout(0.2)(layer4Z)
        layer6 = layers.Dense(2)(layer5)
        outputLayer = layers.Softmax()(layer6)

        self.model = keras.Model(inputs=self.img_inputs, outputs=outputLayer, name='NSFW_classifier')

        pass

    def displayModelSumary(self):
        self.model.summary()
        pass

    def compileTheModel(self):
        lossFunction = tf.keras.losses.CategoricalCrossentropy(from_logits=False)
        optimizer = tf.keras.optimizers.Adam()
        self.model.compile(optimizer=optimizer, loss=lossFunction, metrics=['accuracy'])
        pass

    def createDatasets(self):
        self.train_dataset = tf.data.Dataset.from_tensor_slices((self.x_train, self.y_train)).batch(batch_size=self.train_batch_Size)
        self.val_dataset = tf.data.Dataset.from_tensor_slices((self.x_val, self.y_val)).batch(batch_size=self.val_batch_size)
        self.train_dataset = self.train_dataset.shuffle(32000)
        pass

    def trainTheNetwork(self, numOfIterations):
        self.model.fit(self.train_dataset, epochs=numOfIterations, validation_data=self.val_dataset)
        self.saveWholeModel()
        pass

    def saveWholeModel(self):
        self.model.save(self.saveDestination)
        pass

    def evaluateModel(self):
        self.model.evaluate(self.x_val, self.y_val, batch_size=self.val_batch_size)

    def getModel(self):
        return self.model