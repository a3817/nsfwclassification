import MatrixBuilder as matrix_Builder
import CompleteMatrixBuilder as complete_matrix_builder
import FolderType as ft
import ConvolutionalNeuralNetwork as cnn
import Window as window
import tensorflow as tf
import TrainPreTrainedModel as tptm

trainTheModel = True
trainExistingModel = True

finalModel = None

startingIndex = 0
endingIndex = 10000

if trainTheModel:

    SFWMatrixBuilder = matrix_Builder.MatrixBuilder(folderPath="C:/Users/allan/Desktop/nsfw dataset/train/SFW",
                                                    folderType=ft.FolderType.SFW, startingIndex=startingIndex,
                                                    endingIndex=endingIndex)  # Max images = 121611
    NSFWMatrixBuilder = matrix_Builder.MatrixBuilder(folderPath="C:/Users/allan/Desktop/nsfw dataset/train/NSFW",
                                                     folderType=ft.FolderType.NSFW, startingIndex=startingIndex,
                                                     endingIndex=endingIndex)  # max images = 112373

    SFWListOfCoordinates = SFWMatrixBuilder.getAppendedListOfCoordinates()
    SFWListOfLabels = SFWMatrixBuilder.getAppendedListOfLabels()

    NSFWListOfCoordinates = NSFWMatrixBuilder.getAppendedListOfCoordinates()
    NSFWListOfLabels = NSFWMatrixBuilder.getAppendedListOfLabels()

    completeMatrixBuilder = complete_matrix_builder.CompleteMatrixBuilder(SFWCoordinateList=SFWListOfCoordinates,
                                                                          NSFWCoordinateList=NSFWListOfCoordinates,
                                                                          SFWLabelList=SFWListOfLabels,
                                                                          NSFWLabelList=NSFWListOfLabels
                                                                          )
    completeMatrixBuilder.buildInputMatrix()
    completeMatrixBuilder.buildLabelMatrix()

    compeletedInputMatrix = completeMatrixBuilder.getCompleteInputMatrix()
    completedLabelMatrix = completeMatrixBuilder.getCompleteLabelMatrix()

    if trainExistingModel:
        loadedModel = tf.keras.models.load_model("saved_model/my_model16")
        selectedNetwork = tptm.TrainPreTrainedModel(x_train=compeletedInputMatrix,
                                                    y_train=completedLabelMatrix,
                                                    train_batch_size=128,
                                                    model=loadedModel,
                                                    saveDestination="saved_model/my_model16")
    else:
        selectedNetwork = cnn.ConvolutionalNeuralNetwork(x_train=compeletedInputMatrix,
                                                         y_train=completedLabelMatrix,
                                                         train_batch_size=128,
                                                         saveDestination="saved_model/my_model16")
        selectedNetwork.defineTheModel()
        pass
    selectedNetwork.displayModelSumary()
    selectedNetwork.compileTheModel()
    selectedNetwork.createDatasets()
    selectedNetwork.trainTheNetwork(numOfIterations=1)
    theModel = selectedNetwork.getModel()
else:
    # preTrainedModel = ptm.PreTrainedModel(224) #Dimensions
    # preTrainedModel.defineTheModel()
    # theModel = preTrainedModel.getPreTrainedModel()
    # theModel.load_weights("checkPointDirectory/training_1/cp.ckpt")
    theModel = tf.keras.models.load_model("saved_model/my_model16")
    pass

finalModel = theModel